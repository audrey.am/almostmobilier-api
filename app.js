const express = require('express');
const app = express();
const PORT = process.env.PORT || 4000;
const cors = require('cors');
const bodyParser = require('body-parser');


app.listen(PORT, function(err){
    if(err)console.log("error in server setup")
    console.log("server listening on Port", PORT)
} )
const userRoutes = require ('./utilisateur/utilisateur.route');
const produitRoutes = require('./produit/produit.route')
const devisRoutes = require ('./devis/devis.route')
app.options('*', cors())
app.use(cors({ origin: true, credentials: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/utilisateur', userRoutes);
app.use('/produit', produitRoutes);
app.use('/devis', devisRoutes)

app.get('/', function(req, res){
   
});
app.use(
    express.urlencoded({
      extended: true,
    })
  );


  require("./db");