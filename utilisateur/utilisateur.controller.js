const users = require('./utilisateur.model')

exports.findAll = function(req, res) {
    users.findAll(function(err, user) {
        if(err) res.send(err);
        res.send(user)
    })
}

exports.findOne = function (req, res){
    let user_id = req.params.id;
    
    users.findOne(function(err, user){
        if (err) res.send(err);
        res.send(user)
    }, (user_id))
   
}

exports.addOne= function (req, res){
    users.addOne(req.body)
    res.send(req.body)
    
}

exports.update = function (req, res){
  users.update(req.body, req.params.id)
  res.send(req.body)
    }

exports.delete = function (req, res){
    let user_id = req.params.id;
    users.delete((err, data)=> {
        if (err)
res.status(500).send({
    message: err.message || "Une erreur est survenue",})

else res.send(data);}, user_id)}
    