const express = require ('express');
const router = express.Router();
const utilisateurController = require('./utilisateur.controller.js');

router.get('/', utilisateurController.findAll)
router.post('/', utilisateurController.addOne)
router.get('/:id', utilisateurController.findOne)
router.patch('/:id', utilisateurController.update)
router.delete('/:id', utilisateurController.delete)


module.exports = router