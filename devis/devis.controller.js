const devis = require ('./devis.model')


exports.findAll = function (req, res){
    devis.findAll(function(err, devis){
        if(err)
            res.send('err', err)
        res.send(devis)
    })
}

exports.findOne = function (req, res){
    let devis_id = req.params.id;
    
    devis.findOne(function(err, devis){
        if (err) res.send(err);
        res.send(devis)
    }, (devis_id))
   
}

exports.addOne= function (req, res){
  
    devis.addOne(req.body)
    res.send(req.body)
    
}
exports.update = function (req, res){
    devis.update(req.body, req.params.id)
    res.send(req.body)
}
 exports.delete = function (req, res){
        let user_id = req.params.id;
        devis.delete((err, data)=> {
            if (err)
    res.status(500).send({
        message: err.message || "Une erreur est survenue",})
    
    else res.send(data);}, user_id)}
        