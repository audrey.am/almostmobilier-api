const express = require ('express');
const router = express.Router();

const devisController = require ('./devis.controller')
router.get ('/', devisController.findAll)
router.post('/', devisController.addOne)
router.get('/:id', devisController.findOne)
router.patch('/:id', devisController.update)
router.delete('/:id', devisController.delete)



module.exports = router