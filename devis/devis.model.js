const db = require("../db");

let Devis = function (devis) {
  this.nom_utilisateur = action.nom_utilisateur;
  this.numero_devis = action.numero_devis;
  this.demande = action.demande;
  this.prix_devis = action.prix_devis;
};

Devis.findAll = function (result) {
  let sql =
    "SELECT nom_utilisateur, numero_devis, demande, prix_devis from devis INNER JOIN utilisateur on devis.id_client = utilisateur.id";
  db.query(sql, function (err, res) {
    if (err) {
      result(null, err);
    } else {
      console.log("res dans modelDevis", res);
      result(null, res);
    }
  });
};

Devis.findOne = function (result, numero_devis) {
  let sql = `SELECT  nom_utilisateur, id_client, numero_devis, demande, prix_devis FROM devis INNER JOIN utilisateur on devis.id_client = utilisateur.id WHERE numero_devis = ${numero_devis}`;
  db.query(sql, function (err, res) {
    if (err) {
      console.log(err);
    } else {
      console.log(res);
      result(null, res);
    }
  });
};

Devis.addOne = function (result) {
    console.log(result)

  let sql = `INSERT INTO devis (id_client, demande, prix_devis) VALUES ('${result.id_client}', '${result.demande}', '${result.prix_devis}')`;
  db.query(sql, function (err, res) {
    if (err) {
      console.log(err);
    } else {
      console.log(res);
    }
  });
};


Devis.update = function (resultat, numero_devis){
  console.log(resultat)
  let sql = `UPDATE devis SET id_client='${resultat.id_client}', demande ='${resultat.demande}', prix_devis = '${resultat.prix_devis}' WHERE numero_devis=${numero_devis}`;
  db.query(sql, function(err, res){
    if(err){
      console.log(err);} else{
        console.log(res)
      }
    })
  };

  
 Devis.delete = function (resultat, numero_devis){
    let sql = `DELETE FROM devis WHERE numero_devis=${numero_devis}`;
    db.query(sql, function(err, res){
      if (err) {
      console.log(err);
      resultat(null, err);
      return
    } else {
     resultat(null, res)
    }
  });
  };

module.exports = Devis;
