const produit = require ('./produit.model')

exports.findAll = function(req, res) {
    produit.findAll(function(err, produit){
        if(err)
            res.send('err', err);
        res.send(produit)
    })
}
exports.findAllPlante = function(req, res) {
    produit.findAllPlante(function(err, produit){
        if(err)
            res.send('err', err);
        res.send(produit)
    })
}

exports.findAllTableau = function(req, res) {
    produit.findAllTableau(function(err, produit){
        if(err)
            res.send('err', err);
        res.send(produit)
    })
}
exports.findAllLuminaire = function(req, res) {
    produit.findAllLuminaire(function(err, produit){
        if(err)
            res.send('err', err);
        res.send(produit)
    })
}
exports.findOne = function (req, res){
    let produit_id = req.params.id;
    
    produit.findOne(function(err, produit){
        if (err) res.send(err);
        res.send(produit)
    }, (produit_id)) 
   
}

exports.addOne= function (req, res){
   
    produit.addOne(req.body)
    res.send(req.body)
    
}
 
exports.update = function (req, res){
    console.log('req PARAMS', req.params)
  produit.update(req.body, req.params.id)
  res.send(req.body)
    }

exports.delete = function (req, res){
    let produit_id = req.params.id; 
    produit.delete((err, data)=> {
        if (err)
res.status(500).send({
    message: err.message || "Une erreur est survenue",})

else res.send(data);}, produit_id)}
    