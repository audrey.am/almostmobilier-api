const express = require ('express');

const router = express.Router();
const produitController = require ('./produit.controller')

router.get('/', produitController.findAll)  //Read
router.post('/', produitController.addOne) //Create
router.get('/plante', produitController.findAllPlante)
router.get('/tableau', produitController.findAllTableau)
router.get('/luminaire', produitController.findAllLuminaire)
router.get('/:id', produitController.findOne)
router.patch('/:id', produitController.update)  //update
router.delete('/:id', produitController.delete)  //delete


 
module.exports = router 